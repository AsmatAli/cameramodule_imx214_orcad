|------------------------------------------------------------------------------|
|                                 ECO   REPORT                                 |
|                                                                    Page   1  |
|------------------------------------------------------------------------------|
|  Y:.../Tesseract/Camera_Module/3.PCB/Tesseract_CameraModule.brd              |
|                                                    Fri Sep 20 18:26:30 2019  |
|------------------------------------------------------------------------------|
| COMPONENT DEFINITION added to design                                         |
|- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - |
|    device type                                                               |
|- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - |
 FIDUCIALS_FD_FIDUCIALS
|------------------------------------------------------------------------------|
|                                 ECO   REPORT                                 |
|                                                                    Page   2  |
|------------------------------------------------------------------------------|
|  Y:.../Tesseract/Camera_Module/3.PCB/Tesseract_CameraModule.brd              |
|                                                    Fri Sep 20 18:26:30 2019  |
|------------------------------------------------------------------------------|
| COMPONENTS ADDED to design                                                   |
|- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - |
|    ref des        |    device type                                           |
|- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - |
 FD1                 FIDUCIALS_FD_FIDUCIALS 
 FD2                 FIDUCIALS_FD_FIDUCIALS 
 FD3                 FIDUCIALS_FD_FIDUCIALS 
 FD4                 FIDUCIALS_FD_FIDUCIALS 
|------------------------------------------------------------------------------|
|                                 ECO   REPORT                                 |
|                                                                    Page   3  |
|------------------------------------------------------------------------------|
|  Y:.../Tesseract/Camera_Module/3.PCB/Tesseract_CameraModule.brd              |
|                                                    Fri Sep 20 18:26:30 2019  |
|------------------------------------------------------------------------------|
| SLOT PROPERTIES added to design                                              |
|- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - |
|   slot_id    |   x   |   y   |   property   |             value              |
|- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - |
 FD1.1                          PRIM_FILE      .\pstchip.dat
 FD2.1                          PRIM_FILE      .\pstchip.dat
 FD3.1                          PRIM_FILE      .\pstchip.dat
 FD4.1                          PRIM_FILE      .\pstchip.dat
|------------------------------------------------------------------------------|
|                                 ECO   REPORT                                 |
|                                                                    Page   4  |
|------------------------------------------------------------------------------|
|  Y:.../Tesseract/Camera_Module/3.PCB/Tesseract_CameraModule.brd              |
|                                                    Fri Sep 20 18:26:30 2019  |
|------------------------------------------------------------------------------|
| PIN PROPERTIES added to design                                               |
|- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - |
|    pin_id    |   x   |   y   |   property   |             value              |
|- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - |
 FD1.1                          CDS_PINID      \1\
 FD2.1                          CDS_PINID      \1\
 FD3.1                          CDS_PINID      \1\
 FD4.1                          CDS_PINID      \1\
|------------------------------------------------------------------------------|
|       Comp definitions added:       1                                        |
|       Components added      :       4                                        |
|       Pin property added    :       4                                        |
|       Slot property added   :       4                                        |
|                                                                              |
|   Total ECO changes reported:      13                                        |
|------------------------------------------------------------------------------|
